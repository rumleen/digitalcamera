/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package digitalcamera;

/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public abstract class DigitalCamera {
    String name;
    String model;
    Double megapixel;
    String make;
    
    DigitalCamera(String name, String model, double megapixel, String make) {
        this.name = name;
        this.model = model;
        this.megapixel = megapixel; 
        this.make = make;
    }
    
    public abstract String describeCamera();
}
