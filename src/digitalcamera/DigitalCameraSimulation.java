/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package digitalcamera;

/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class DigitalCameraSimulation {
    public static void main (String[] args) {
        DigitalCamera pc = new PhoneCamera("Apple", "I Phone ", 6, "Apple", 64);
        DigitalCamera psc = new PhoneCamera("Canon", "Powershot A590 ", 8, "Cannon", 16);
        System.out.println(pc.describeCamera());
        System.out.println(psc.describeCamera());
    }
}
