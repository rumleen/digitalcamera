/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package digitalcamera;

/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public abstract class PointAndShootCamera extends DigitalCamera {
    private double external;
    
    public PointAndShootCamera (String name, String model, double megaPixel, String make, double external) {
        super(name, model, megaPixel, make);
        this.external = external;
    }
    
    @Override
    public String describeCamera() {
        return "Make " + make + " Model = " + model + " Megapixel" + megapixel + " Storage = " + external;
    }
}
