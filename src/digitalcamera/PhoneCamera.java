/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package digitalcamera;

/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class PhoneCamera extends DigitalCamera {
    private double internal;
    public PhoneCamera(String name, String model, double megaPixel, String make, double internal){
        super(name, model, megaPixel, make);
        this.internal = internal;
    }
    
    @Override
    public String describeCamera() {
        return "Make " + make + " Model = " + model + " Megapixel" + megapixel + " Storage = " + internal;
    }
    
}
